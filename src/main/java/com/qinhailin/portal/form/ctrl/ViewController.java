/**
 * Copyright 2019-2024 覃海林(qinhaisenlin@163.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

package com.qinhailin.portal.form.ctrl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.qinhailin.common.base.BaseController;
import com.qinhailin.common.config.WebContant;
import com.qinhailin.common.vo.Grid;
import com.qinhailin.common.vo.TreeNode;
import com.qinhailin.portal.core.service.DataSourceService;
import com.qinhailin.portal.form.service.FormViewService;
import com.qinhailin.portal.form.service.SysTreeService;
import com.qinhailin.portal.generator.service.CodeService;


/**
 * 在线表单设计
 * @author QinHaiLin
 * @date 2019年4月24日  
 */
@Path("/portal/form/view")
public class ViewController extends BaseController {

	@Inject
	FormViewService service;
	@Inject
	SysTreeService sysTreeService;
	@Inject
	DataSourceService dataSourceService;
	@Inject
	CodeService codeService;
	
	/**
	 * 表单首页
	 * 
	 * @author QinHaiLin
	 * @date 2019年4月24日
	 */
	public void index() {
		render("index.html");
	}
	
	/**
	 * 表单分类树
	 * 
	 * @author QinHaiLin
	 * @date 2019年4月24日
	 */
	public void tree() {
		Collection<TreeNode> nodeList = sysTreeService.getSysTree(getPara("type"),null);
		Collection<TreeNode> nodes = new ArrayList<TreeNode>();
		TreeNode node = new TreeNode();
		node.setId("");
		node.setText(WebContant.projectName);
		node.setChildren(nodeList);
		nodes.add(node);
		renderJson(nodes);
	}
	
	/**
	 * 表单查询列表
	 * 
	 * @author QinHaiLin
	 * @date 2019年4月24日
	 */
	public void list() {
		renderJson(service.queryPage(getParaToInt("pageNumber"),getParaToInt("pageSize"),getKv()));
	}
	
	public void add() {
		setAttr("treeId", getPara("treeId"));
		setAttr("treeName", getPara("treeName"));
		render("add.html");
	}
		
	public void save() {	
		Record record=getAllParamsToRecord();
		if(service.isExit("code", record.get("code"))){
			renderJson(fail("编号已存在，请重新输入！"));
			return;	
		}
		
		record.set(service.getPrimaryKey(), createUUID());		
		record.set("create_time", new Date()).set("status", "INIT");		
		boolean b=service.save(record);
		if(!b) {
			renderJson(fail());
			return;
		}
		renderJson(ok());
	}
	
	public void edit() {
		render("edit.html");
	}
	
	public void getModel() {
		Record formView=service.findById(getPara(0));
		if(formView==null) {
			renderJson(fail("对象数据不存在"));
			return;
		}
		
		//查询所属分类树名称
		Record sysTree=sysTreeService.findById(formView.get("tree_id"));
		if(sysTree!=null) {
			String treeName=sysTree.get("name");
			formView.set("tree_name", treeName);
		}
		renderJson(ok(formView));
	}
	
	public void update() {
		Record record=getAllParamsToRecord();
		record.set("update_time", new Date());
		boolean b=service.update(record);
		if(!b) {
			renderJson(fail());
			return;
		}
		renderJson(ok());
	}
	
	public void delete() {
		boolean b=service.deleteByIds(getIds());
		renderJson(b?suc():err());
	}
	
	public void updateStatus() {
		Record record=getAllParamsToRecord();
		record.set("update_time", new Date());
		boolean b=service.update(record);
		if(!b) {
			renderJson(fail());
			return;
		}
		renderJson(ok());
	}
	
	/**
	 * 数据库表
	 */
	public void tables(){
		set("dataSourceList",dataSourceService.getDataSourceListByStart(1));
		render("tables.html");
	}
	
	/**
	 * 生成代码
	 * @author QinHaiLin
	 * @date 2020-02-21
	 */
	public void createCode(){
		Record record=getAllParamsToRecord();
		String[] tableNameArray=getPara("name").split(",");
		boolean b=false;
		for(int i=0;i<tableNameArray.length;i++){
			// 查询表信息
			record.set("name", tableNameArray[i]+"=");
			Grid grid=codeService.queryTablesList(record,getSession());
			@SuppressWarnings("unchecked")
			List<TableMeta> tableList=(List<TableMeta>) grid.getList();
			List<ColumnMeta> columnMetas=tableList.get(0).columnMetas;
			
			// 用模板引擎生成 HTML 片段 
			Ret ret=Ret.by("columnMetas", columnMetas);
			String objectName=tableNameArray[i].replaceFirst("w_", "").replaceFirst("W_", "");
			ret.set("objectName",objectName);
			ret.set("primaryKey",tableList.get(0).primaryKey);
			String formItem = renderToString("temp/_form.html", ret);
			String tableItem = renderToString("temp/_table.html", ret);
			ret.set("formItem",formItem);
			ret.set("tableItem", tableItem);
			ret.set("configName", record.get("configName"));
			ret.set("tableComment",record.get("tableComment"));
			// 创建模板内容
			String addHtml=renderToString("temp/add.html", ret);
			String editHtml=renderToString("temp/edit.html", ret);
			String indexHtml=renderToString("temp/index.html", ret);
			//保存模板数据
			record.set("addHtml", addHtml);
			record.set("editHtml", editHtml);
			record.set("indexHtml", indexHtml);
			record.set("objectName", objectName);
			b=service.createCodeTemplete(record);
		}
		
		renderJson(b ? ok() : fail());	
	}
}
